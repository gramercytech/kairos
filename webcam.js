$(document).ready(function() {
  console.log('doc loaded');

  var uniquePeople = 0;
  var alreadySeen = 0;

  // starting the video stream
  var streaming = false;
  var video = null;

  video = document.getElementById('videoElement');
  navigator.getUserMedia({
      video: true,
      audio: false
    },
    function(stream) {
      if (navigator.mozGetUserMedia) {
        video.mozSrcObject = stream;
      } else {
        var vendorURL = window.URL || window.webkitURL;
        video.srcObject = stream
      }
      video.play();
      localStream = stream.getTracks()[0];
    },
    function(err) {
      console.log(err)
    }
  );

  // analyze a frame every 3 sec
  $('#start').click(function() {
    console.log('clicked begin');
    // make request
    var threeSec = setInterval(function() {
      console.log("Analyzing Frame");
      takepicture(video);
    }, 3000);

    $('#stop').click(function() {
      console.log('pressed stop')
      clearInterval(threeSec);
    })
  })

  // manually compare a single frame against all known saved faces
  $('#compare').click(function() {
    console.log('clicked compare')
    var canvas = document.createElement('CANVAS');
    var context = canvas.getContext('2d');
    canvas.width = '600';
    canvas.height = '450';
    context.drawImage(video, 0, 0);
    var imageData = canvas.toDataURL('image/png');
    console.log(imageData)
    $('#compared-pic').html("<img src='" + imageData + "'>");
    comparePicture(imageData)
  })

  // gallery
  var gal;
  $('#gallery').click(function() {
    console.log('clicked gallery')
    var compareUrl = "https://api.kairos.com/gallery/view";
    var payload = {
      "gallery_name": "test_gallery"
    };
    // make request
    $.ajax(compareUrl, {
      headers: headers,
      type: "POST",
      data: JSON.stringify(payload),
      dataType: "text"
    }).done(function(response) {
      console.log(JSON.parse(response));
      var res = JSON.parse(response);
      gal = res.subject_ids;
    });
  })

  // clear gallery
  $('#remove').click(function() {
    console.log('clear gallery')
    var compareUrl = "https://api.kairos.com/gallery/remove_subject";

    gal.forEach(function(data, index) {
      var payload = {
        "gallery_name": "test_gallery",
        "subject_id": data
      };
      // make request
      $.ajax(compareUrl, {
        headers: headers,
        type: "POST",
        data: JSON.stringify(payload),
        dataType: "text"
      }).done(function(response) {
        console.log(JSON.parse(response));
      });
    });

  })

  // 1) take photo
  // 2) compare against gallery of photos
  // 3) if new face add to gallery and + unique views, if already seen, do nothing

  var headers = {
    "Content-type": "application/json",
    "app_id": "5a83cff4",
    "app_key": "ebd16358ae2b9a7e31b806fa6163bd0a"
  };

  var detectURL = "https://api.kairos.com/detect";

  var takepicture = function(video) {
    var canvas = document.createElement('CANVAS');
    var context = canvas.getContext('2d');
    canvas.width = '600';
    canvas.height = '450';
    context.drawImage(video, 0, 0);
    var imageData = canvas.toDataURL('image/png');
    $('#analyzed-pic').html("<img src='" + imageData + "'>");
    comparePicture(imageData);
  }

  var sendPicture = function(image) {
    var payload = {
      "image": image
    };
    $.ajax(detectURL, {
      headers: headers,
      type: "POST",
      data: JSON.stringify(payload),
      dataType: "text"
    }).done(function(response) {
      console.log(response);
      var res = JSON.parse(response);
      $('#results').html(response)
      var filename = res.images[0]['file'].substring(0, res.images[0]['file'].lastIndexOf("."));
      filename = filename.replace("_","");
      console.log('filename', filename)
      processPicture(image, filename);
      // compare picture, if it's not a match, then process picture
    });
  }

  // adds the face to the test_gallery
  var processPicture = function(image, filename) {
    var payload = {
      "image": image,
      "gallery_name": "test_gallery",
      "subject_id": filename
    };
    var enrollUrl = "https://api.kairos.com/enroll";
    // make request
    $.ajax(enrollUrl, {
      headers: headers,
      type: "POST",
      data: JSON.stringify(payload),
      dataType: "text"
    }).done(function(response) {
      console.log(JSON.parse(response));
      var res = JSON.parse(response);
      // emotionDetectPicture(res.face_id)
    });
  }

  // compare against images in test_gallery
  var comparePicture = function(image) {
    var payload = {
      "image": image,
      "gallery_name": "test_gallery"
    };
    var compareUrl = "https://api.kairos.com/recognize";
    // make request
    $.ajax(compareUrl, {
      headers: headers,
      type: "POST",
      data: JSON.stringify(payload),
      dataType: "text"
    }).done(function(response) {
      console.log(JSON.parse(response));
      var res = JSON.parse(response);
      $('#compared-res').html(response)
      if(res.images[0]['transaction']['status'] === 'failure'){
        console.log('failure')
        uniquePeople++;
        sendPicture(image)
        $('#count').html(uniquePeople);
      } else if (res.images[0]['transaction']['status'] === 'success'){
        console.log('success')
        alreadySeen++;
        $('#seencount').html(alreadySeen);
      }
    });
  }

  // var emotionDetectPicture = function (image) {
  //   var analyzeURL = "https://api.kairos.com/v2/media?source=";
  //   $.ajax(analyzeURL, {
  //     headers: headers,
  //       type: "POST",
  //   }).done(function(response){
  //     console.log(JSON.stringify(response));
  //     $('#analyzed-results').html(JSON.stringify(response));
  //   });
  // }


})
