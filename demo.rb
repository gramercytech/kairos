#!/usr/bin/ruby

require 'net/http'
require 'uri'
require 'json'

uri = URI("https://api.kairos.com/detect")

# put your keys in the header
headers = {
  "Content-type": "application/json",
  "app_id": "5a83cff4",
  "app_key": "ebd16358ae2b9a7e31b806fa6163bd0a"
}

payload = {"image": "https://media.kairos.com/liz.jpg"}

# create the HTTP objects
http = Net::HTTP.new(uri.host, uri.port)
request = Net::HTTP::Post.new(uri.request_uri, headers)
request.body = payload.to_json

# make request
response = http.request(request)

print response.body